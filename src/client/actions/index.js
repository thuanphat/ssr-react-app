import axios from 'axios';

export const FETCH_USERS = 'fetch_users';

export const fetchUsers = () => async dispatch => {
  let url = 'https://us-central1-ssr-react-app-de762.cloudfunctions.net/api/users';
  const res = await axios.get(url);

  dispatch({
    type: FETCH_USERS,
    payload: res.data
  });
};
