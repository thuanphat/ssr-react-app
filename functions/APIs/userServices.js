exports.getAllUser = (request, response) => {
    todos = [
        {
            'id': '1',
            'firstName': 'Thao',
            'lastName': 'Nguyen Duy',
            'email':'duythao.ndt@gmail.com',
            'phoneNumber':'0868937490',
            'dob':'16/12/1990',
            'address':'Bac Ninh'
        },
        {
            'id': '2',
            'firstName': 'Sam',
            'lastName': 'Vu Quang',
            'email':'samvq@gmail.com',
            'phoneNumber':'086893440',
            'dob':'01/02/1990',
            'address':'Bac Ninh'
        },
        {
            'id': '3',
            'firstName': 'Linh',
            'lastName': 'Nguyen Huy',
            'email':'linhnh@gmail.com',
            'phoneNumber':'0868937470',
            'dob':'10/10/1992',
            'address':'Ha Noi'
        },
        {
            'id': '4',
            'firstName': 'Phuc',
            'lastName': 'Hoang Nguyen',
            'email':'phuchn@gmail.com',
            'phoneNumber':'0868937498',
            'dob':'16/08/1991',
            'address':'Thai Binh'
        },
        {
            'id': '5',
            'firstName': 'Tien',
            'lastName': 'Dinh Manh Tien',
            'email':'tiendm@gmail.com',
            'phoneNumber':'0868937120',
            'dob':'16/02/1991',
            'address':'Ninh Binh'
        },
        {
            'id': '6',
            'firstName': 'Hien',
            'lastName': 'Nguyen Thi Thu',
            'email':'hienntt2@gmail.com',
            'phoneNumber':'0868937421',
            'dob':'06/08/1992',
            'address':'Ninh Binh'
        },
        {
            'id': '7',
            'firstName': 'Huong',
            'lastName': 'Dang Thi',
            'email':'huongdt3@gmail.com',
            'phoneNumber':'086891110',
            'dob':'07/08/1993',
            'address':'Ninh Binh'
        }
    ]
    return response.json(todos);
}