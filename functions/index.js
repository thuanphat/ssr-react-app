const functions = require('firebase-functions');
const app = require('express')();

const {
    getAllUser
} = require('./APIs/userServices')

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/users', getAllUser);
exports.api = functions.https.onRequest(app);