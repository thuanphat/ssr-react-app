### Installing
Git clone project and install dependencies
$ git clone 
$ cd ssr-react-app
$ npm install

## Run on local
### Build project
$ npm run dev
### Run project on local
Comment line at file ./src/index.js
 //export const ssrapp = functions.https.onRequest(app);
Uncomment lines at file ./src/index.js
 app.listen(port, () => {
   console.log(`Listening on port: ${port}`);
 });
And start on local
$ npm run dev
$ npm start 
Navigate to [http://localhost:3000]

## Deployment
Uncomment line at file ./src/index.js
 //export const ssrapp = functions.https.onRequest(app);
Comment lines at file ./src/index.js
 //app.listen(port, () => {
 // console.log(`Listening on port: ${port}`);
 //});
 $ npm run dev
 And deploy on firebase hosting
$ firebase deploy