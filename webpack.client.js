const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base');

const config = {
  mode: 'development',
  entry: './src/client/client.js',
  output: {
    filename: 'client.js',
    path: path.resolve(__dirname, 'build')
  },
  devtool: 'inline-source-map'
};

module.exports = merge(baseConfig, config);
